package com.rest.demo.server.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.rest.demo.server.entity.Student;
import com.rest.demo.server.model.StudentResponse;
import com.rest.demo.server.repository.StudentRepository;

@Component
public class DemoDaoImpl implements DemoDao {

	@Autowired
	StudentRepository studentRepository;

	@Override
	public StudentResponse getStudents(Integer pageSize, Integer pageNo) {
		pageNo = (pageNo == null) ? 0 : pageNo;
		pageSize = (pageSize == null) ? 20 : pageSize;

		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by("admNo").ascending());

		Page<Student> listData = studentRepository.findAll(pageable);
		StudentResponse studentResponse = new StudentResponse(listData.getTotalPages(), listData.toList());
		return studentResponse;
	}

	@Override
	public List<Student> searchStudents(String keyword) {
		List<Student> listData = studentRepository.findByBatchCode(keyword);
		return listData;
	}

}
