package com.rest.demo.server.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "students")
public class Student {

	@Id
	@Column(name = "ADMNO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long admNo;

	@Column(name = "BATCHCODE")
	private String batchCode;
	@Column(name = "ROLLNO")
	private int rollNo;
	@Column(name = "FULLNAME")
	private String fullName;
	@Column(name = "FATHERNAME")
	private String fatherName;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "PHONE")
	private String phone;
	@Column(name = "DJ")
	private Date dateOfJoin;

	public Long getAdmNo() {
		return admNo;
	}

	public void setAdmNo(Long admNo) {
		this.admNo = admNo;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	@Override
	public String toString() {
		return "Student [admNo=" + admNo + ", batchCode=" + batchCode + ", rollNo=" + rollNo + ", fullName=" + fullName
				+ ", fatherName=" + fatherName + ", email=" + email + ", phone=" + phone + ", dateOfJoin=" + dateOfJoin
				+ "]";
	}

}
