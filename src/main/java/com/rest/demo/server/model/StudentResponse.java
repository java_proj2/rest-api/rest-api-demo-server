package com.rest.demo.server.model;

import java.util.List;

import com.rest.demo.server.entity.Student;

public class StudentResponse {

	private int totalPage;
	private List<Student> student;

	public StudentResponse(int totalPage, List<Student> student) {
		super();
		this.totalPage = totalPage;
		this.student = student;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public List<Student> getStudent() {
		return student;
	}

	public void setStudent(List<Student> student) {
		this.student = student;
	}

}
