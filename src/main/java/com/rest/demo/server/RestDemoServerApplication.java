package com.rest.demo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.rest.demo.server.repository"})
@EntityScan(basePackages = {"com.rest.demo.server.entity"})
@ComponentScan({"com.rest.demo.server"})
public class RestDemoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestDemoServerApplication.class, args);
	}
}