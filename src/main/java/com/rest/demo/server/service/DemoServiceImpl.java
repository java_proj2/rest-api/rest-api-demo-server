package com.rest.demo.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rest.demo.server.dao.DemoDao;
import com.rest.demo.server.entity.Student;
import com.rest.demo.server.model.StudentResponse;

@Service
public class DemoServiceImpl implements DemoService {

	@Autowired
	DemoDao demoDao;
	
	@Override
	public StudentResponse getStudents(Integer pageSize, Integer pageNo) {
		return demoDao.getStudents(pageSize, pageNo);
	}

	@Override
	public List<Student> searchStudents(String keyword) {
		return demoDao.searchStudents(keyword);
	}

}
