package com.rest.demo.server.service;

import java.util.List;

import com.rest.demo.server.entity.Student;
import com.rest.demo.server.model.StudentResponse;


public interface DemoService {

	StudentResponse getStudents(Integer pageSize, Integer pageNo);

	List<Student> searchStudents(String keyword);
	
}
