package com.rest.demo.server.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rest.demo.server.entity.Student;
import com.rest.demo.server.model.StudentResponse;
import com.rest.demo.server.service.DemoService;

@RestController
@CrossOrigin
@RequestMapping({ "/rest/demo" })
public class RestDemoController {

	@Autowired
	DemoService demoService;

	@GetMapping(value = "/getStudent", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getStudents(@RequestParam(value = "page", required = false) final Integer pageNo,
			@RequestParam(value = "size", required = false) final Integer pageSize) {
		StudentResponse studentResp = demoService.getStudents(pageSize, pageNo);
		return ResponseEntity.status(HttpStatus.OK).body(studentResp);
	}

	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> searchStudents(@RequestParam(value = "keyword") String keyword,
			@RequestParam(value = "page", required = false) final Integer pageNo,
			@RequestParam(value = "size", required = false) final Integer pageSize) {
		List<Student> list = new ArrayList<>();
		if (keyword.trim().isEmpty()) {
		} else {
			list = demoService.searchStudents(keyword.trim());
		}

		return ResponseEntity.status(HttpStatus.OK).body(list);
	}

}
